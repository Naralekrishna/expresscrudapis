const mongoose = require('mongoose');

const tourSchema = mongoose.Schema({
    title: { type: String, required: true},
    description: { type: String, required: true},
    imageUrl: { type: String, required: true},
    userId: { type: Number, default: 1, required: true},
    price: { type: Number, required: true}
});

module.exports = mongoose.model('Tour', tourSchema);
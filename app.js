// Core Modules
const express = require('express');
// Developer Modules
require('./configuration/mongoose');
const Tour = require('./models/tour');
const tourRoutes = require('./routes/tour');

// -------------------------------------------

// Creating App Instance
const app = express();
// Port number where app will run
const port = 3000;
// Connect to DB

// Middelwares
app.use(express.json());

// Base route of application
app.get('/', (req, res) => {
    res.send('App is running');
});
// Tours Routes
app.use('/api/v1/tours/', tourRoutes);

app.listen(port, () => {
    console.log(`App is running on ${port}...`);
})
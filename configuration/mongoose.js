const mongoose = require('mongoose');

// Making mongoose promise to global promise
mongoose.Promise = global.Promise;

mongoose.connect('mongodb://127.0.0.1:27017/demo', { useFindAndModify: false, useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: true, useFindAndModify: true })
.then(() => { console.log('Db Connected') })
.catch((err) => { console.log(err)});

module.exports = mongoose;